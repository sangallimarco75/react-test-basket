import React, { Component } from 'react';
import './app.css';
import ShippingBasket from './shopping-basket/components/shopping-basket';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ShippingBasket />
      </div>
    );
  }
}

export default App;
