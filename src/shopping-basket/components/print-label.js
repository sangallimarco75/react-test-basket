import React from 'react';
import StreamComponent from '../../core/libs/stream-component';
import Barcode from 'react-barcode';

export default class PrintLabel extends StreamComponent {

    constructor(props) {
		super(props);

        let rate = 0.5;
        let cost = (this.props.packageWeight * rate).toFixed(2); // rate here
        let code = new Date().getTime().toString();
        this.state = {
            cost,
            code
        };
	}

    handlePrint(evt) {
        window.print();
    }

    render() {
		return (
			<div>
                <Barcode value={this.state.code} />
                <div className="shipping-label-maker-label">Sender: {this.props.senderAddress}</div>
                <div className="shipping-label-maker-label">Receiver: {this.props.receiverAddress}</div>
                <div className="shipping-label-maker-label">Weight: {this.props.packageWeight} Kg</div>
                <div className="shipping-label-maker-label">Cost: {this.state.cost} CHF</div>
                <button className="shipping-label-maker-button" onClick={(evt)=>this.handlePrint()}>Print</button>
            </div>
		);
	}


}

