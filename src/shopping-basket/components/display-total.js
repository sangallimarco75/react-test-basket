import React from 'react';
import PropTypes from 'prop-types';
import {Components} from './';
import StreamComponent from '../../core/libs/stream-component';
import './display-total.css';
import Format from '../../core/libs/format';

export default class DisplayTotal extends StreamComponent {

    handleClick(evt) {
        // dispatch new state
        this.streams.CHANGE.next({
            id: Components.CHECKOUT,
            data: {
            }
        });
    }

    render() {
        let {total} = this.props;

		return (
            <div className="total">
                <div className="total-col total-label">TOTAL: {Format.toCurrency(total)} CHF</div>
                <button className="total-col total-button" onClick={(evt)=>this.handleClick(evt)}>CHECKOUT</button>
            </div>
		);
	}

}

// propTypes
DisplayTotal.propTypes = {
    total: PropTypes.number
};