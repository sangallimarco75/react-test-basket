import React from 'react';
import {StreamComponent} from '../../core/libs';
import {Components, Streams} from './';
import BasketService from '../services';
import Item from './item';
import ItemList from './item-list';
import DisplayTotal from './display-total';

import './shopping-basket.css';

export default class ShoppingBasket extends StreamComponent {

    constructor(props) {
		super(props);

        this.state= {
            items: [],
            products: [],
            total: 0,
            edit: true
        };

        // dispatcher
        let actions = {
            [Components.ITEM]: x => {
                let {id, quantity} = x.data;
                
                this.basket.upsert(id, quantity);
                let items = this.basket.getItems();
                let total = this.basket.total();
                this.setState({...this.state, items, total});
            },
            [Components.ITEM_LIST]: x => {
                let {uuid} = x.data;
                
                this.basket.remove(uuid);
                let items = this.basket.getItems();
                let total = this.basket.total();
                this.setState({...this.state, items, total});
            },
            [Components.CHECKOUT]: x => {
                let edit = false;
                this.setState({...this.state, edit});
            }
        };

        this.sm.dispatch(Streams.CHANGE, actions);

        // @TODO remove this, testing only
        Streams.CHANGE.subscribe(
            x=>console.log('>>>',x)
        );

        // init BasketService
        this.basket = new BasketService();
        this.basket
            .init()
            .then(
                products=>{
                    console.log('Data loaded');
                    this.setState(Object.assign(this.state, {
                        products
                    }));
                }
            );
	}

    renderItem(uuid, label, quantity, total, icon) {
        let {edit} = this.state;

        return (<ItemList key={uuid} uuid={uuid} label={label} quantity={quantity} total={total} options={this.state.products} icon={icon} edit={edit} streams={Streams}/>);
    }

    renderEdit(){
        let {products, edit} = this.state;

        if (products.length && edit) {
            return (<Item uuid="*" options={products} streams={Streams}/>);
        }
        return null;
    }

    render() {
        let {total, items, edit} = this.state;

		return (
            <div className="basket">
                <DisplayTotal total={total} streams={Streams}/>
                <h1> ITEMS </h1>
                <div className="list">
                    {
                        this.renderEdit()
                    }
                    <div className="separator"></div>
                    {
                        items.map(item=>{
                            let {uuid, label, quantity, total, icon} = item;
                            return this.renderItem(uuid, label, quantity, total, icon);
                        })
                    }
                </div>
                
            </div>
		);
	}

}

