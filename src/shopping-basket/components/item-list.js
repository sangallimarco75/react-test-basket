import React from 'react';
import PropTypes from 'prop-types';
import {Components} from './';
import StreamComponent from '../../core/libs/stream-component';
import './item.css';
import Format from '../../core/libs/format';
import Icon from 'react-svg-use';

export default class ItemList extends StreamComponent {

    handleClick(evt) {
        let {uuid} = this.props;
        // dispatch new state
        this.streams.CHANGE.next({
            id: Components.ITEM_LIST,
            data: {
                uuid
            }
        });
    }

    render() {
        let {label, total, quantity, icon, edit} = this.props;
        let formattedLabel = quantity > 1 ? Format.pluralize(label) : label;

		return (
            <div className="item">
                <div className="item-fixed">
                    <Icon className="icon" id={icon} />
                </div>
                <div className="item-inline">[{quantity}] {formattedLabel}</div>
                <div className="item-inline">{Format.toCurrency(total)} CHF</div>
                <button className="item-fixed button danger" disabled={!edit} onClick={(evt)=>this.handleClick(evt)}>✕</button>
            </div>
		);
	}

}

// propTypes
ItemList.propTypes = {
    total: PropTypes.number,
    quantity: PropTypes.number,
    uuid: PropTypes.string,
    streams: PropTypes.object,
    label: PropTypes.string,
    icon: PropTypes.string,
    edit: PropTypes.bool
};