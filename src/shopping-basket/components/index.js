import {Stream} from '../../core/libs';

const Components = {
    START: 'START',
    ITEM: 'ITEM',
    ITEM_LIST: 'ITEM_LIST',
    PRINT_LABEL: 'PRINT_LABEL',
    CHECKOUT: 'CHECKOUT'
};

const Streams = {
	INJECT: new Stream(),
	CHANGE: new Stream()
};

export {
    Components,
    Streams
};