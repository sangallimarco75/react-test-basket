import React from 'react';
import PropTypes from 'prop-types';
import {Components} from './';
import StreamComponent from '../../core/libs/stream-component';
import './item.css';

const UNSELECTED = "*";

export default class Item extends StreamComponent {

    constructor(props){
        super(props);

        this.state = {
            quantity: this.props.quantity,
            id: this.props.id // component can be reused
        };

        this.defaultOption = this.props.options[0];

    }

    resetState() {
        let quantity = 1;
        let id = UNSELECTED;
        this.setState({...this.state, quantity, id});
    }

    handleClick(evt) {
        // dispatch new state
        this.streams.CHANGE.next({
            id: Components.ITEM,
            data: this.state
        });
        
        // reset quantity and select
        this.resetState();
    }

    handleChange(evt) {
        let quantity = evt.target.value ? parseInt(evt.target.value, 10) : 0;
        this.setState({...this.state, quantity});
    }

    handleProductSelect(evt) {
        let id = evt.target.value;
        this.setState({...this.state, id});
    }

    renderOption(id, label) {
        return (<option key={id} value={id} >{label}</option>);
    }

    render() {
        let {quantity, id} = this.state;
        let disabled = id === UNSELECTED || !quantity;

		return (
            <div className="item">
                <div className="item-col">
                    <label>Product</label>
                    <select className="input" onChange={(evt)=>this.handleProductSelect(evt)} value={id}>
                        {
                            this.renderOption(UNSELECTED, "Please Select...")
                        }
                        {
                            this.props.options.map(option => this.renderOption(option.id, option.label)) 
                        }
                    </select>
                </div>
                <div className="item-col">
                    <label>Quantity</label>
                    <input className="input" type="number" min="1" max="30" step="1" placeholder="Quantity..." onChange={(evt)=>this.handleChange(evt)} value={quantity}/>
                </div>
                <div className="item-col">
                    <label>Quantity</label>
                    <button className="button" disabled={disabled} onClick={(evt)=>this.handleClick(evt)}>Add</button>
                </div>
            </div>
		);
	}
}

// propTypes
Item.propTypes = {
    id: PropTypes.string,
    quantity: PropTypes.number,
    uuid: PropTypes.string,
    options: PropTypes.array,
    streams: PropTypes.object
};

Item.defaultProps = {
    quantity: 1,
    id: UNSELECTED
};
