import CONFIG from './config.json';
import uuidV4 from 'uuid/v4';

export default class BasketService {

    constructor() {
        this.items = [];
        this.config = [];
    }

    /**
     * Emulate an API call and get products
     */
    init() {
        // @TODO API call here
        return new Promise((resolve, reject) => {
            this.config = CONFIG;
            resolve(CONFIG);
        });
    }

    /**
     * Get product by id
     */
    getProduct(id) {
        return this.config.find(x => x.id === id);
    }

    /**
     * Calculate price based on special discounts
     */
    calculatePrice(quantity, price, split, pay) {
        return quantity % split ? price * quantity : price * quantity * (pay / split);
    }

    /**
     * Create item struct
     */
    createItem(product, quantity) {
        let {
            split,
            price,
            pay
        } = product;

        let total = this.calculatePrice(quantity, price, split, pay);
        return Object.assign({
            uuid: uuidV4(),
            quantity,
            total
        }, product);
    }

    /**
     * Merge quantities
     */
    mergeItem(product, item, quantity) {
        let newQuantity = item.quantity + quantity;
        let {
            split,
            price,
            pay
        } = product;

        let total = this.calculatePrice(newQuantity, price, split, pay);
        return { ...item,
            quantity: newQuantity,
            total
        };
    }

    /**
     * Use product id in order to merge quantities
     */
    upsert(id, quantity) {
        let product = this.getProduct(id);

        // upsert here
        let indx = this.items.findIndex(x => x.id === id);

        // create doc
        let item = {};

        if (indx > -1) {
            let matched = this.items[indx];
            item = this.mergeItem(product, matched, quantity);

            let {
                uuid
            } = matched;
            this.remove(uuid);
        } else {
            item = this.createItem(product, quantity);
        }

        // move products to the top
        this.items.unshift(item);
        return true;
    }

    /**
     * Remove by uuid
     */
    remove(uuid) {
        this.items = this.items.filter(x => x.uuid !== uuid);
        return true;
    }

    /**
     * Reduce and calculate total
     */
    total() {
        let totals = this.items.map((x) => x.total);
        return totals.length ? totals.reduce((a, x) => a + x) : 0;
    }

    /**
     * Get items clone
     */
    getItems() {
        return this.items.map((x) => Object.assign({}, x));
    }
}