import pluralize from 'pluralize';

export default class Format {
    static toCurrency(value) {
        return (value / 100).toFixed(2);
    }
    static pluralize(value) {
        return pluralize(value);
    }
}